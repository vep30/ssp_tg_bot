import random
import telebot
from telebot import types

answers = ["Камень", "Ножницы", "Бумага"]
bot = telebot.TeleBot("token_from_tg")


@bot.message_handler(commands=["start"])
def handle_start(message):
    keyboard = types.ReplyKeyboardMarkup(True)
    stone = types.KeyboardButton("Камень")
    scissors = types.KeyboardButton("Ножницы")
    paper = types.KeyboardButton("Бумага")
    keyboard.add(stone, scissors, paper)

    bot.send_message(message.chat.id, get_greetings(message), reply_markup=keyboard)


@bot.message_handler(func=lambda message: True)
def handle_message(message):
    answer = random.choice(answers)
    mes_text = message.text
    if (stone_scissors(mes_text, answer)) or (scissors_paper(mes_text, answer)) or (paper_stone(mes_text, answer)):
        result = "Вы победили!"
    elif message.text == answer:
        result = "ничья!"
    else:
        result = "Вы проиграли!"

    bot.reply_to(message, f"У меня {answer.lower()}. {get_user_name(message)}, {result}")


def get_greetings(message):
    return f"Доброго времени суток, {get_user_name(message)}!"


def get_user_name(message):
    if message.from_user.first_name is not None:
        return message.from_user.first_name
    return "Аноним"


def stone_scissors(mes_text, answer):
    if mes_text == "Камень" and answer == "Ножницы":
        return True


def scissors_paper(mes_text, answer):
    if mes_text == "Ножницы" and answer == "Бумага":
        return True


def paper_stone(mes_text, answer):
    if mes_text == "Бумага" and answer == "Камень":
        return True


bot.polling(none_stop=True)
