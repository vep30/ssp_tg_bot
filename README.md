# SSP_TG_Bot

Небольшой телеграм-бот для игры в "камень-ножницы-бумага" ("stone-scissors-paper" - SSP).

- Для начала нужно установить библиотеку pyTelegramBotAPI командой:

    `pip install pyTelegramBotAPI`

    или

    `pip install pytelegrambotapi`

- Чтобы бот работал, в строку "bot = telebot.TeleBot("token_from_tg")" вместо "token_from_tg", нужно вставить токен, полученный от бота "BotFather".

    Пример:
    bot = telebot.TeleBot("0000000000:AAAAAaaAAAA0_aaaaaAAAaaaaaaaAAaaaaa")
